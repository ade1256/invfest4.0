<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Team;
use App\UserHasTeam;
use App\Mentor;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = User::where('id', auth()->id())->first();
        if ($role->role == 1) {
            return redirect('/administrator/dashboard');
        }
        $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        $cek_mentor = Mentor::where('id_user', auth()->id())->first();
        if($cek_user !== null || $cek_mentor !== null){
            return redirect('/participants/dashboard');
        }else{
            return view('home');
        }
    }
}
