<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Team;
use App\UserHasTeam;
use App\Mentor;
use App\Announcement;

class AdministratorController extends Controller
{
    public function index()
    {
        $user = User::where('id',auth()->id())->first();
        if($user->role == 1) {
            return view('admin.index');
        }
        return redirect()->back();
    }

    public function announcement()
    {
        $user = User::where('id',auth()->id())->first();
        if($user->role == 1) {
            $announcement_view = Announcement::all();
            return view('admin.announcement',compact('announcement_view'));
        }
        return redirect()->back();
    }

    public function addAnnouncement(Request $request)
    {
        $request->validate([
            'announcement' => 'required',
        ]);

        Announcement::create([
            'announcement' => $request->announcement,
        ]);

        return redirect('/administrator/announcement')->with('success', 'Berhasil ditambahkan !');
    }
}
