<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\User;
use App\Team;
use App\UserHasTeam;
use App\Mentor;
use DB;

class TeamController extends Controller
{
    public function createTeam(Request $request)
    {       
        $request->validate([
            'team_name' => 'required',
            'category' => 'required',
        ]);

        $token = str_random(8);

        Team::create([
            'team_name' => $request->team_name,
            'leader_id' => auth()->id(),
            'category' => $request->category,
            'token' => $token,
        ]);

        $getidTeam = DB::table('teams')->orderBy('id_team', 'DESC')->first();

        UserHasTeam::create([
            'id_team' => $getidTeam->id_team,
            'id_user' => auth()->id()            
        ]);

        return redirect('/participants/profile')->with('success', 'Tim Berhasil Dibuat, Lengkapi data dibawah ini untuk melanjutkan !');
    }

    public function joinTeam(Request $request)
    {       
        $request->validate([
            'token' => 'required',
            'roleaccess' => 'required',
        ]);
        
        if($request->roleaccess == 1)  {
            //Member
            $team = Team::where('token', $request->token)->first();
            if($team !== null){
                $userJoinTeam = UserHasTeam::where('id_team', $team->id_team)->get();
                $status = count($userJoinTeam) < 3 ? 'bisa' : 'penuh';
                if($status === 'bisa'){
                    UserHasTeam::create([
                        'id_team' => $team->id_team,
                        'id_user' => auth()->id(),
                    ]);
                    return redirect('/participants/profile')->with('success', 'Berhasil bergabung dengan TIM, Lengkapi data dibawah ini untuk melanjutkan !');
                } else {
                    return redirect('/participants')->with('warning', 'Team sudah penuh.');
                }
            }else{
                return redirect('/participants')->with('danger', 'Team tidak di temukan.');
            }

        } elseif($request->roleaccess == 2) {
            //Pembimbing
            $team = Team::where('token', $request->token)->first();
            if($team !== null){
                $userJoinTeam = Mentor::where('id_team', $team->id_team)->get();
                $status = count($userJoinTeam) < 1 ? 'bisa' : 'penuh';
                if($status === 'bisa'){
                    Mentor::create([
                        'id_team' => $team->id_team,
                        'id_user' => auth()->id(),
                    ]);
                    return redirect('/participants/profile')->with('success', 'Berhasil bergabung dengan TIM, Lengkapi data dibawah ini untuk melanjutkan !');
                } else {
                    return redirect('/participants')->with('warning', 'Team sudah penuh.');
                }
            }else{
                return redirect('/participants')->with('danger', 'Team tidak di temukan.');
            }
        } else {
            return redirect()->back();
        }
    }
    
    public function outTeam()
    {
        $outTeam = UserHasTeam::where('id_user',auth()->id())->delete();
        return redirect('/participants');
    }
    
    public function deleteTeam()
    {
        $team           = Team::where('leader_id',auth()->id())->first();
        $deleteMember   = UserHasTeam::where('id_team',$team->id_team);
        if($deleteMember->count() > 0) {
            $deleteMember->delete();
        }
        $deleteMentor   = Mentor::where('id_team',$team->id_team);
        if($deleteMentor->count() > 0){
            $deleMentor->delete();
        }
        $deleteTeam     = Team::where('id_team',$team->id_team)->delete();
        return redirect('/participants');
    }

    public function updateTeamName(Request $request)
    {
        $request->validate([
            'team_name' => 'required',
        ]);
        
        $team = Team::where('leader_id', auth()->id())->first();
        if ($team !== null) {
            $teamupdate = Team::where('id_team', $team->id_team);
            $teamupdate->update([
                'team_name' => $request->team_name,
            ]);
            return redirect('/participants/team')->with('success', 'Sukses merubah nama Tim !');
        } else {
            return redirect('/participants/team')->with('info', 'Hanya ketua Tim yang dapat merubah !');
        }
    }

    public function givePayment(Request $request)
    {
        $request->validate([
            'payment' => 'required|file|mimes:pdf'
        ]);

        if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
            $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        } else {
            $cek_user = Mentor::where('id_user', auth()->id())->first();
        }
        $team = Team::where('id_team', $cek_user->id_team)->first();
        
        if($team->category == 'uiux'){
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_UI_UX_EXPLORATION_LEGALITY_PAYMENT.'.$request->payment->getClientOriginalExtension();
        } else {
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_APP_INNOVATION_PAYMENT.'.$request->payment->getClientOriginalExtension();
        }
        
        Storage::makeDirectory('data');

        $path = $request->file('payment')->storeAs(
            'data/' . $team->id_team , $fileName
        );
        
        $payment = Team::where('id_team', $team->id_team);
        $payment->update([
            'payment' => $fileName,
            'status' => 'process',
        ]);

        return redirect('/participants/payment')->with('info', 'Bukti pembayaran telah dikirim, tunggu konfirmasi dari admin !');
    }

    public function giveLegality(Request $request)
    {
        $request->validate([
            'legality' => 'required|file|mimes:pdf'
        ]);

        if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
            $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        } else {
            $cek_user = Mentor::where('id_user', auth()->id())->first();
        }
        $team = Team::where('id_team', $cek_user->id_team)->first();
        
        if($team->category == 'uiux'){
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_UI_UX_EXPLORATION_LEGALITY.'.$request->legality->getClientOriginalExtension();
        } else {
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_APP_INNOVATION_LEGALITY.'.$request->legality->getClientOriginalExtension();
        }

        Storage::makeDirectory('data');

        $path = $request->file('legality')->storeAs(
            'data/' . $team->id_team , $fileName
        );
        
        $payment = Team::where('id_team', $team->id_team);
        $payment->update([
            'poes' => $fileName,
        ]);

        return redirect('/participants/dashboard')->with('info', 'Leglitas berhasil di upload !');
    }

    public function giveProposal(Request $request)
    {
        $request->validate([
            'proposal' => 'required|file|mimes:pdf'
        ]);

        if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
            $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        } else {
            $cek_user = Mentor::where('id_user', auth()->id())->first();
        }
        $team = Team::where('id_team', $cek_user->id_team)->first();
        
        if($team->category == 'uiux'){
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_UI_UX_EXPLORATION_PROPOSAL.'.$request->proposal->getClientOriginalExtension();
        } else {
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_APP_INNOVATION_PROPOSAL.'.$request->proposal->getClientOriginalExtension();
        }

        Storage::makeDirectory('data');

        $path = $request->file('proposal')->storeAs(
            'data/' . $team->id_team , $fileName
        );
        
        $payment = Team::where('id_team', $team->id_team);
        $payment->update([
            'proposal' => $fileName,
        ]);

        return redirect('/participants/dashboard')->with('info', 'Proposal berhasil di upload !');
    }

    public function giveApplication(Request $request)
    {
        $request->validate([
            'application' => 'required|file|mimes:zip'
        ]);

        if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
            $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        } else {
            $cek_user = Mentor::where('id_user', auth()->id())->first();
        }
        $team = Team::where('id_team', $cek_user->id_team)->first();

        if($team->category == 'uiux'){
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_UI_UX_EXPLORATION_APPLICATION.'.$request->application->getClientOriginalExtension();
        } else {
            $fileName = 'INVFEST4_2019_'.$team->team_name.'_APP_INNOVATION_APPLICATION.'.$request->application->getClientOriginalExtension();
        }
        
        Storage::makeDirectory('data');

        $path = $request->file('application')->storeAs(
            'data/' . $team->id_team , $fileName
        );
        
        $payment = Team::where('id_team', $team->id_team);
        $payment->update([
            'application' => $fileName,
        ]);

        return redirect('/participants/dashboard')->with('info', 'Aplikasi berhasil di upload !');
    }

}
