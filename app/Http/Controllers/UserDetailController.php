<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Team;
use App\UserHasTeam;
use App\Mentor;

class UserDetailController extends Controller
{
    public function updateParticipants(Request $request)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'no_hp' => 'required||min:5||max:15',
            'asal_pendidikan' => 'required',
        ]);
        
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $user->name = $request->nama_lengkap;
        $user->no_hp = $request->no_hp;
        $user->asal_pendidikan = $request->asal_pendidikan;
        $user->save();

        return redirect('/participants/profile')->with('success', 'Profile berhasil di update !');
    }
}
