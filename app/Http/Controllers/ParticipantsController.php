<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Team;
use App\UserHasTeam;
use App\Mentor;
use App\Announcement;
use DB;

class ParticipantsController extends Controller
{

    public function participants()
    {
        $role = User::where('id', auth()->id())->first();
        if ($role->role == 1) {
            return redirect('/administrator/dashboard');
        }
        $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        $cek_mentor = Mentor::where('id_user', auth()->id())->first();
        if($cek_user !== null || $cek_mentor !== null){
                if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
                    $id_team = UserHasTeam::where('id_user', auth()->user()->id)->first();
                } else {
                    $id_team = Mentor::where('id_user', auth()->user()->id)->first();
                }
                $team = Team::where('id_team', $id_team->id_team)->first();
                
                $userPopulate = UserHasTeam::join('users', 'users.id', '=' , 'user_has_team.id_user')
                            ->where('id_team', $id_team->id_team)
                            ->get();
                $mentorPopulate = Mentor::join('users', 'users.id', '=' , 'mentors.id_user')
                            ->where('id_team', $id_team->id_team)
                            ->get();
                $teamPopulate = Team::join('user_has_team','teams.id_team' , '=' , 'user_has_team.id_team')
                            ->where('teams.id_team', $id_team->id_team)
                            ->get();
                $leader = $teamPopulate->first()->leader_id;
                
                            // dd($teamPopulate);
                if ($team->status === 'actived') {
                    return view('participants.home', compact('team','userPopulate','teamPopulate','leader','mentorPopulate'));
                } elseif ($team->status === 'process') {
                    return redirect('/participants/payment')->with('info', 'Pembayaran sedang di proses !');
                } else {
                    return redirect('/participants/payment')->with('danger', 'Silahkan lakukan pembayaran terlebih dahulu !');
                }
        }else{
            return redirect()->back();
        }
    }

    public function event()
    {
        $role = User::where('id', auth()->id())->first();
        if ($role->role == 1) {
            return redirect('/administrator/dashboard');
        }
        $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        $cek_mentor = Mentor::where('id_user', auth()->id())->first();
        if($cek_user !== null || $cek_mentor !== null){ 
            $announcement_view = Announcement::all();
            return view('participants.event',compact('announcement_view'));
        }else{
            return redirect()->back();
        }
    }

    public function team()
    {
        $role = User::where('id', auth()->id())->first();
        if ($role->role == 1) {
            return redirect('/administrator/dashboard');
        }
        $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        $cek_mentor = Mentor::where('id_user', auth()->id())->first();
        if($cek_user !== null || $cek_mentor !== null){
            if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
                $id_team = UserHasTeam::where('id_user', auth()->user()->id)->first();
            } else {
                $id_team = Mentor::where('id_user', auth()->user()->id)->first();
            }
            $team = Team::where('id_team', $id_team->id_team)->first();
            return view('participants.team', compact('team'));
        }else{
            return redirect()->back();
        }
    }

    public function profile()
    {       
        $role = User::where('id', auth()->id())->first();
        if ($role->role == 1) {
            return redirect('/administrator/dashboard');
        }
        $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        $cek_mentor = Mentor::where('id_user', auth()->id())->first();
        if($cek_user !== null || $cek_mentor !== null){
            if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
                $id_team = UserHasTeam::where('id_user', auth()->user()->id)->first();
            } else {
                $id_team = Mentor::where('id_user', auth()->user()->id)->first();
            }
            $team = Team::where('id_team', $id_team->id_team)->first();
            return view('participants.profile', compact('team'));
        }else{
            return redirect()->back();
        }
    }

    public function payment()
    {
        $role = User::where('id', auth()->id())->first();
        if ($role->role == 1) {
            return redirect('/administrator/dashboard');
        }
        $cek_user = UserHasTeam::where('id_user', auth()->id())->first();
        $cek_mentor = Mentor::where('id_user', auth()->id())->first();
        if($cek_user !== null || $cek_mentor !== null){
            $cek_data_diri = user::where('id', auth()->id())->first();
            if ($cek_data_diri->no_hp !== null && $cek_data_diri->asal_pendidikan !== null) {
                if(UserHasTeam::where('id_user', auth()->user()->id)->first() !== null) {
                    $id_team = UserHasTeam::where('id_user', auth()->user()->id)->first();
                } else {
                    $id_team = Mentor::where('id_user', auth()->user()->id)->first();
                }
                $team = Team::where('id_team', $id_team->id_team)->first();
                return view('participants.payment',compact('team'));
            } else {  
                return redirect('/participants/profile')->with('info', 'Lengkapi Data Diri Anda !');
            }
        }else{
            return redirect()->back();
        }
    }

}
