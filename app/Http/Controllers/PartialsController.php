<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PartialsController extends Controller
{
    public function index()
    {
        return view('coomingsoon');
    }
    public function app_inv()
    {
        return view('app_innovation');
    }
    public function uiux()
    {
        return view('ui_ux_exploration');
    }
}
