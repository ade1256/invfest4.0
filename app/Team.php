<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    protected $primaryKey = 'id_team';
    protected $fillable = ['team_name', 'token', 'leader_id', 'category', 'payment', 'poes', 'proposal', 'application','status']; 

    public function Team()
    {
        return $this->belongsTo(User::class);
    }

    public function Mentor()
    {
        return $this->belongsTo(Mentor::class);
    }

    public function Participant()
    {
        return $this->belongsTo(UserHasTeam::class);
    }

}
