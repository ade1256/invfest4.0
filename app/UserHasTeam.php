<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasTeam extends Model
{
    protected $table = 'user_has_team';
    protected $fillable = ['id_team', 'id_user']; 
    
    public function hasTeam()
    {
        return $this->belongsTo(User::class);
    }

    public function toTeam()
    {
        return $this->hasMany(Team::class);
    }

}
