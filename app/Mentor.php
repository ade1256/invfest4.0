<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentor extends Model
{
    protected $table = 'mentors';
    
    protected $fillable = ['id_user', 'id_team']; 
    
    public function Mentors()
    {
        return $this->belongsTo(User::class);
    }

    public function MentorToTeam()
    {
        return $this->hasMany(Team::class);
    }
}
