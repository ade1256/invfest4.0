@extends('layouts.app')
@extends('layouts.navbar-default')

@section('title')
    Participants
@endsection

@section('content')
<div class="container">
    
    <center>
        <img src="{{asset('images/logo-2x.png')}}" alt="Brand Logo" class="m-5">
    </center>
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card card-primary card-outline">
                <div class="card-header">Create Team</div>
                <form role="form" action="{{ route('createTeam') }}" method="post">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group has-feedback{{ $errors->has('team_name') ? ' has-error' : ''}}">
                            <label for="team_name">Nama TIM</label>
                            @if($errors->has('team_name'))
                                <div class="callout callout-danger">
                                    <p>{{ $errors->first('team_name') }}</p>
                                </div>
                            @endif
                            <input type="text" class="form-control" id="team_name" name="team_name" placeholder="Nama Tim" value="{{ old('team_name') }}">
                        </div>
                        <div class="form-group has-feedback{{ $errors->has('category') ? ' has-error' : ''}}">
                            <label for="namatim">Kategori TIM</label>
                            @if($errors->has('category'))
                                <div class="callout callout-danger">
                                    <p>{{ $errors->first('category') }}</p>
                                </div>
                            @endif
                            <select class="form-control" name="category">
                                <option value="">Pilih Kategori</option>
                                <option value="app_inv">APP INNOVATION (MAHASISWA)</option>
                                <option value="uiux">UI/UX Exploration (SMA/Sederajat)</option>
                            </select>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-danger card-outline">
                <div class="card-header">Join Team</div>
                <form role="form" action="{{ route('joinTeam') }}" method="post">
                    {{ csrf_field() }}
                    <div class="card-body">
                        @include('partials._alerts')
                        <div class="form-group has-feedback{{ $errors->has('token') ? ' has-error' : ''}}">
                            <label for="tokentim">TOKEN TIM</label>
                            @if($errors->has('token'))
                                <div class="callout callout-danger">
                                    <p>{{ $errors->first('token') }}</p>
                                </div>
                            @endif
                            <input type="text" class="form-control" id="tokentim" placeholder="( Dapatkan dari ketua Tim )" name="token" value="{{ old('token') }}">
                            <label ></label>
                        </div>
                        <div class="form-group">
                            <label for="roleaccess"><i>Role Access</i></label>
                            <select class="form-control has-feedback{{ $errors->has('roleaccess') ? ' has-error' : ''}}" name="roleaccess">
                                @if($errors->has('roleaccess'))
                                    <div class="callout callout-danger">
                                        <p>{{ $errors->first('roleaccess') }}</p>
                                    </div>
                                @endif
                                <option value="">Role Access</option>
                                <option value="1">Anggota</option>
                                <option value="2">Pembimbing (Opsional untuk APP Innovation Wajib untuk UI/UX Exploration)</option>
                            </select>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
