@extends('layouts.master')

@section('title')
    Event
@endsection

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Event</h1>
            </div>
        </div>
    </div> 
</section>



<div class="row">

    <div class="col-lg-4">

        <div class="card">
            <div class="card-body p-5">

                <button type="button" class="btn btn-block btn-outline-info btn-lg">
                    <i class="fa fa-download fa-2x"></i><br>Download Rulebook
                </button>

            </div>
        </div>

        <div class="card">
            <div class="card-header no-border">
                <h3 class="card-title">Timeline</h3>
            </div>
            <div class="card-body">

                <div class="d-flex justify-content-between align-items-center border-bottom mb-1">
                    <p class="text-info">
                        <i class="fa fa-calendar"></i><b> 1 - 31 Agustus</b>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">Pendaftaran Gelombang 1</span>
                    </p>
                </div>

                <div class="d-flex justify-content-between align-items-center border-bottom mb-1">
                    <p class="text-info">
                        <i class="fa fa-calendar"></i><b> 1 - 30 September</b>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">Pendaftaran Gelombang 2</span>
                    </p>
                </div>

                <div class="d-flex justify-content-between align-items-center border-bottom mb-1">
                    <p class="text-danger">
                        <i class="fa fa-calendar"></i><b> 1 Oktober</b>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">Batas Pengumpulan Proposal & Aplikasi</span>
                    </p>
                </div>

                <div class="d-flex justify-content-between align-items-center border-bottom mb-1">
                    <p class="text-primary">
                        <i class="fa fa-calendar"></i><b> 2 - 30 Oktober</b>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">Penilaian dan Penyisihan</span>
                    </p>
                </div>

                <div class="d-flex justify-content-between align-items-center border-bottom mb-1">
                    <p class="text-info">
                        <i class="fa fa-calendar"></i><b> 8 November</b>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">Pengumuman Finalis</span>
                    </p>
                </div>

                <div class="d-flex justify-content-between align-items-center">
                    <p class="text-success">
                        <i class="fa fa-calendar"></i><b> 23 November</b>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">Finalis & Awarding</span>
                    </p>
                </div>

            </div>
        </div>

    </div>

    <div class="col-lg-8">
        <div class="card">
            <div class="card-header bg-info">
                <h3 class="card-title">Pengumuman</h3>
            </div>

            <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <tr>
                        <th>No</th>
                        <th>Keterangan</th>
                    </tr>
                    @foreach ($announcement_view as $announ)
                    <tr>
                        <td> 1 </td>
                        <td> {{ $announ->announcement }} </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

</div>

@endsection


