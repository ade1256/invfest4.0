@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-lg-12">
            <center>
                <img src="{{asset('images/logo-2x.png')}}" alt="Brand Logo" class="mb-5">
            </center>

            @include('partials._alerts')
            </div>
        </div>
    </div> 
</section>



<div class="row">

    <div class="col-lg-6">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title">
                    STATUS TIM :
                </h3>
                <!-- tools box -->
                <div class="card-tools">
                    <button class="btn btn-primary btn-flat btn-sm disabled" style="text-transform: uppercase;">{{$team->status}}</button>
                </div>
            </div>
            
            <div class="card-body mb-3 text-center">
                <span class="text-small mt-3">Team :</span>
                <h2>{{ $team->team_name }}</h2>
                <span class="text-small mt-3">Token :</span>
                <div class="input-group" style="postion:absolute;margin:0 auto;width:50%;" >
                    <input style="text-align:center;font-size:32px;border:none;" type="text" class="form-control" type="text" id="textToken" value="{{ $team->token }}" disabled="true">
                    <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat" onclick="copyText()" title="Salin" data-toggle="tooltip"><span class="fa fa-copy"></button>
                    </span>
                </div>
            </div>

            <div class="card-footer mt-4">
                <p style="font-style:italic;">Note : Rahasiakan dan hanya berikan kepada anggota tim anda !</p>
            </div>

        </div> 

    </div>

    <div class="col-lg-6">

        <div class="card">
            <div class="card-body">

                <div class="d-flex justify-content-between align-items-center border-bottom">
                    <p class="text-info text-xl">
                        <i class="ion ion-android-checkmark-circle"></i>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">VERIFIKASI EMAIL</span>
                    </p>
                </div>
                
                <div class="d-flex justify-content-between align-items-center border-bottom">
                    <p class="text-info text-xl">
                        <i class="ion ion-android-checkmark-circle"></i>
                    </p>
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">STATUS PEMBAYARAN</span>
                    </p>
                </div>
                
                <div class="d-flex justify-content-between align-items-center border-bottom">
                    @if($team->poes !== null)
                    <p class="text-info text-xl">
                        <i class="ion ion-android-checkmark-circle"></i>
                    </p>
                    @else 
                    <p class="text-danger text-xl">
                        <i class="ion ion-alert-circled"></i>
                    </p>
                    @endif
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">SUBMIT LEGALITAS</span>
                    </p>
                </div>
                
                <div class="d-flex justify-content-between align-items-center border-bottom">
                    @if($team->proposal !== null)
                    <p class="text-info text-xl">
                        <i class="ion ion-android-checkmark-circle"></i>
                    </p>
                    @else 
                    <p class="text-danger text-xl">
                        <i class="ion ion-alert-circled"></i>
                    </p>
                    @endif
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">SUBMIT PROPOSAL</span>
                    </p>
                </div>
                
                <div class="d-flex justify-content-between align-items-center mb-1">
                    @if($team->application !== null)
                    <p class="text-info text-xl">
                        <i class="ion ion-android-checkmark-circle"></i>
                    </p>
                    @else 
                    <p class="text-danger text-xl">
                        <i class="ion ion-alert-circled"></i>
                    </p>
                    @endif
                    <p class="d-flex flex-column text-right">
                        <span class="text-muted">SUBMIT APLIKASI</span>
                    </p>
                </div>
                
            </div>
        </div>
        
    </div>

    <div class="col-lg-4">

        <div class="callout callout-success">
            <h5>Legalitas !</h5>
            <p>Silahkan Upload Legalitas anda.</p>
        </div>
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Legalitas</h3>
            </div>
            <form role="form" action="{{ route('giveLegality') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        @if($errors->has('legality'))
                            <div class="callout callout-danger">
                                <p>{{ $errors->first('legality') }}</p>
                            </div>
                        @endif
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="legality">
                            </div>
                        </div>
                        <p style="font-style:italic;">Note : File harus berupa *.PDF !</p>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>

            </form>
        </div>   

    </div>

    <div class="col-lg-4">

        <div class="callout callout-primary">
            <h5>Proposal !</h5>
            <p>Silahkan Upload Proposal anda.</p>
        </div>
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Proposal</h3>
            </div>
            <form role="form" action="{{ route('giveProposal') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        @if($errors->has('proposal'))
                            <div class="callout callout-danger">
                                <p>{{ $errors->first('proposal') }}</p>
                            </div>
                        @endif
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="proposal">
                            </div>
                        </div>
                        <p style="font-style:italic;">Note : File harus berupa *.PDF !</p>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>    
    </div>

    <div class="col-lg-4">

        <div class="callout callout-info">
            <h5>Karya !</h5>
            <p>Silahkan Upload Karya anda.</p>
        </div>
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Karya</h3>
            </div>
            <form role="form" action="{{ route('giveApplication') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        @if($errors->has('application'))
                            <div class="callout callout-danger">
                                <p>{{ $errors->first('application') }}</p>
                            </div>
                        @endif
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="application">
                            </div>
                        </div>
                        <p style="font-style:italic;">Note : File harus berupa *.ZIP !</p>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                </div>

            </form>
        </div>   

    </div>
    
    @foreach($mentorPopulate as $dataMentor)
    <div class="col-lg-3 col-xs-12">

        <div class="card card-widget widget-user-2">
            
            <div class="widget-user-header bg-primary">
                <h3>{{$dataMentor->name}}</h3>
                <h5>Pembimbing</h5>
            </div>
            
            <div class="card-footer p-0">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Nama Tim <span class="float-right badge bg-primary">{{$team->team_name}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Asal Sekolah/Institusi <span class="float-right badge bg-info">{{$dataMentor->asal_pendidikan}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Mata Lomba <span class="float-right badge bg-success">{{ $team->category == 'uiux' ? 'UI/UX Exploration' : 'APP Innovation' }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            No.hp <span class="float-right badge bg-warning">{{$dataMentor->no_hp}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Email <span class="float-right badge bg-danger">{{$dataMentor->email}}</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
        
    </div>
    @endforeach

    @foreach($userPopulate as $dataUser)
    <div class="col-lg-3 col-xs-12">

        <div class="card card-widget widget-user-2">
            <div class="widget-user-header {{ $dataUser->id_user == $leader ? 'bg-danger' : 'bg-info' }}">           
                <h3>{{$dataUser->name}}</h3>
                <h5>
                @if($dataUser->id_user == $leader)
                    Ketua
                @else
                    Anggota
                @endif          
                </h5>
            </div>
            
            <div class="card-footer p-0">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Nama Tim <span class="float-right badge bg-primary">{{$team->team_name}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Asal Sekolah/Institusi <span class="float-right badge bg-info">{{$dataUser->asal_pendidikan}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Mata Lomba <span class="float-right badge bg-success">{{ $team->category == 'uiux' ? 'UI/UX Exploration' : 'APP Innovation' }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            No.hp <span class="float-right badge bg-warning">{{$dataUser->no_hp}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            Email <span class="float-right badge bg-danger">{{$dataUser->email}}</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
        
    </div>
    @endforeach

</div>

@endsection


