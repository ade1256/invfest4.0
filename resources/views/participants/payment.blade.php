@extends('layouts.master')

@section('title')
    Payment
@endsection

@section('content')

<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">

        <center>
            <img src="{{asset('images/logo-2x.png')}}" alt="Brand Logo" class="mt-5 mb-5">
        </center>

        @include('partials._alerts')

        <div class="card card-primary">
            <div class="card-header no-border">
                <h3 class="card-title">Pembayaran</h3>
                <div class="card-tools">
                    Status : <button class="btn btn-sm btn-@if($team->status=='unactive')danger
                    @elseif($team->status=='process')warning 
                    @elseif($team->status=='actived')success
                    @endif disbaled" style="text-transform: uppercase;font-weight:bold;color:white;">{{$team->status}}</button>
                </div>
            </div>

            @if($team->status=='actived')
                <div class="card-body m-5 text-center">
                    <i class="ion ion-android-checkmark-circle mb-4" style="font-size:56px;color:#007bff"></i>
                    <h1>Terima kasih sudah melakukan Pembayaran :) </h1>
                </div>
            @else
                <div class="mt-5 text-center">
                    <p style="font-size:18px;font-style:italic;">Pembayaran dapat dilakukan melalui rekening <strong>BNI a/n Firda Millennianita 0822224971</strong></p>
                </div>
            <form role="form" action="{{ route('givePayment') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="form-group has-feedback{{ $errors->has('payment') ? ' has-error' : ''}}">
                        <label for="exampleInputFile">File input</label>
                            @if($errors->has('payment'))
                                <div class="callout callout-danger">
                                    <p>{{ $errors->first('payment') }}</p>
                                </div>
                            @endif
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="payment">
                            </div>
                        </div>
                        <p style="font-style:italic;">Note : Lakukan pembayaran dan tunggu konfirmasi dari admin. File harus berupa *.PDF !</p>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
            @endif
            
        </div>
        
    </div>

</div>

@endsection


