@extends('layouts.master')

@section('title')
    Profile
@endsection

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 col-md-12 col-xs-12">
                @include('partials._alerts')
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <h1>Profile</h1>
            <div class="col-lg-3"></div>
            </div>
        </div>
    </div> 
</section>



<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">

        <div class="card card-primary card-outline">

            <form role="form" action="{{ route('updateParticipants') }}" method="post">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="namaLengkap">Nama Lengkap</label>
                        <input type="text" class="form-control" id="namaLengkap" placeholder="Nama Lengkap" value="{{ Auth::user()->name }}" name="nama_lengkap">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" disabled="true" placeholder="{{ Auth::user()->email }}">
                    </div>
                    <div class="form-group">
                        <label for="nohp">No.Hp</label>
                        @if($errors->has('no_hp'))
                            <div class="callout callout-danger">
                                <p>{{ $errors->first('no_hp') }}</p>
                            </div>
                        @endif
                        <input type="number" class="form-control" id="nohp" placeholder="Nomor HP" name="no_hp" value="{{ Auth::user()->no_hp }}">
                    </div>
                    <div class="form-group">
                        <label for="kategori">Kategori Lomba</label>
                        <input type="text" class="form-control" id="kategori" disabled="true" 
                        placeholder="@if($team->category == 'app_inv')APP Innovation 
                        @elseif( $team->category == 'uiux')UI/UX Exploration
                        @endif
                        ">
                    </div>
                    <div class="form-group">
                        <label for="institusi">Asal Sekolah/Institusi</label>
                        @if($errors->has('asal_pendidikan'))
                            <div class="callout callout-danger">
                                <p>{{ $errors->first('asal_pendidikan') }}</p>
                            </div>
                        @endif
                        <input type="text" class="form-control" id="asal_pendidikan" placeholder="Asal Sekolah/Institusi" name="asal_pendidikan" value="{{ Auth::user()->asal_pendidikan }}">
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
            
        </div>

    </div>

</div>

@endsection


