@extends('layouts.master')

@section('title')
    Team
@endsection

@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                @include('partials._alerts')
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <h1>Team</h1>
            <div class="col-lg-3"></div>
            </div>
        </div>
    </div> 
</section>



<div class="row">

    <div class="col-lg-3"></div>
    <div class="col-lg-6">

        <div class="card card-primary card-outline">
        
            <center class="mt-3">
                <h3>Token Tim :</h3>
                <div class="input-group" style="postion:absolute;margin:0 auto;width:50%;" >
                    <input style="text-align:center;font-size:32px;border:none;" type="text" class="form-control" type="text" id="textToken" value="{{ $team->token }}" disabled="true">
                    <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat" onclick="copyText()" title="Salin" data-toggle="tooltip"><span class="fa fa-copy"></button>
                    </span>
                </div>
            </center>

            <form role="form" action="{{route('updateTeamName')}}" method="post">
                {{ csrf_field() }}
                <div class="card-body">
                    @if($errors->has('team_name'))
                        <div class="callout callout-danger">
                            <p>{{ $errors->first('team_name') }}</p>
                        </div>
                    @endif
                    <input name="team_name" class="form-control form-control-lg" type="text" placeholder="Nama Tim" value="{{ $team->team_name }}">

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    
                </div>
           </form> 
                    
        </div>

        <div class="col-lg-12">
            @if($team->leader_id == Auth::user()->id)
            <button class="btn btn-danger" data-toggle="modal" data-target="#hapustim" style="width:100%;">
                Hapus TIM
            </button>
            @else
            <button class="btn btn-danger" data-toggle="modal" data-target="#keluartim" style="width:100%;">
                Keluar TIM
            </button>
            @endif
        </div>
            
    </div>

</div>
           
@if($team->leader_id == Auth::user()->id)         
<div class="modal fade" id="hapustim">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">WARNING !!!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                
            <form role="form" action="{{route('deleteTeam')}}" method="post">

                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <h5>Yakin ingin menghapus TIM ??</h5>
                        <h6>Seluruh informasi TIM termasuk berkas pendaftaran, anggota dan pembimbing akan dihapus dari TIM !!!</h6>
                    </div>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
            
            </form>

        </div>
        
    </div>

</div>
@else
<div class="modal fade" id="keluartim">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">WARNING !!!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                
            <form role="form" action="{{route('outTeam')}}" method="post">

                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <h5>Yakin ingin keluar dari  TIM ??</h5>
                    </div>
                </div>

                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
            
            </form>

        </div>
        
    </div>

</div>
@endif

@endsection


