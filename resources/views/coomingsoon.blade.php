<!DOCTYPE HTML>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <title>INVFEST 4.0 |  Cooming Soon</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPoppins:400,500" rel="stylesheet">
	
	<style>
		body{
			background-image:url('{{asset('frontend/images/soon.svg')}}');
			background-size:cover;
			background-attachment: fixed;
		}
	</style>
</head>
<body>
	<!-- NO CONTENT -->
</body>
</html>