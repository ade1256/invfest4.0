@extends('layouts.app')

@section('title')
    Register
@endsection

@section('content')
<div class="auth">
    <div class="side-left">
        <h1>SELAMAT DATANG !</h1>
        <p>Sudah Punya Akun ?</p>
        <a href="{{ route('login') }}" class="button">SIGN IN</a>
    </div>
    <div class="side-right">
        <h2>SIGN UP</h2>
        <p>Daftarkan Tim Kamu dan Jadilah Juara di INVFEST 4.0 !</p>
        <form method="POST" action="{{ route('register') }}"class="form-register">
            @csrf
            <div class="form-group">
                <span class="icon name"></span>
                <input type="text" placeholder="{{ __('Name') }}" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                    <span style="color:red;">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group">
                <span class="icon email"></span>
                <input type="email" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                    <span style="color:red;">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group">
                <span class="icon password"></span>
                <input type="password" placeholder="{{ __('Password') }}" name="password" required autocomplete="new-password">
                @error('password')
                    <span style="color:red;">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group">
                <span class="icon cpassword"></span>
                <input type="password" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required autocomplete="new-password">
            </div>

            <div class="form-group">
                <button type="submit" class="button pink">SIGN UP</button>
            </div>
        </form>
        <div class="copyright">&copy; INVFEST 2019. All rights reserved.</div>
    </div>
</div>
@endsection