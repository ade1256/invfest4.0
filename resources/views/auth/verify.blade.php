@extends('layouts.app')
@extends('layouts.navbar-default')

@section('title')
    Verification
@endsection

@section('content')
<div class="container">
    
    <center>
        <img src="{{asset('images/logo-2x.png')}}" alt="Brand Logo" class="m-5">
    </center>
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-primary card-outline">
                <div class="card-header">{{ __('Verifikasi Alamat Email Anda') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Tautan verifikasi baru telah dikirim ke alamat email Anda.') }}
                        </div>
                    @endif

                    {{ __('Sebelum melanjutkan, silahkan periksa email Anda dan klik tautan verifikasi.') }}
                    {{ __('Jika kamu tidak menerima email') }}, <a href="{{ route('verification.resend') }}">{{ __('klik di sini untuk meminta tautan verifikasi baru') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
