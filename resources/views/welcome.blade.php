<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Basic Page Needs -->
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>INVFEST 4.0 - 2019 IT Telkom Purwokerto</title>
    
    <meta name="author" content="invfest">
    
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/revolution/css/settings.css')}}">
    
    <!-- Boostrap style -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/bootstrap.css')}}">
    
    <!-- Icommon icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/icommon.css')}}">
    
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/style.css')}}">
    
    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/responsive.css')}}">
    
    <!-- Carousel -->
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/owl.carousel.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/owl.theme.default.min.css')}}"> -->
        
        <!-- Flipster -->
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/flipsternavtabs.css')}}">
            <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/jquery.flipster.min.css')}}"> -->
            
            <!-- Animation Style -->
            <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/animate.css')}}">
            
            <!-- FancyBox -->
            <link rel="stylesheet" type="text/css" href="{{asset('frontend/fancybox/jquery.fancybox.css')}}" media="screen">
            
            <!-- Jquery -->
            <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
            <script src="{{asset('frontend/js/counter.js')}}"></script>
            
            <style>
                .btn-rulebook {
                    height: 40px;
                    line-height: 40px;
                    width: 160px;
                    border: 1px solid #f03c6f;
                    border-radius: 20px;
                    color: #f03c6f;
                    font-size: 15px;
                    display: inline-block;
                    margin-top: 35px;
                }
                .btn-rulebook:hover {
                    background-color: #f03c6f;
                    color: #ffffff;
                }
                .timelineDot {
                    margin:20px 20px !important; 
                }
                .timelineDate {
                    margin-left:50px !important; 
                    margin-right:50px !important; 
                }
                .timelineWork {
                    margin-left:50px!important; 
                }
                .icon-title {
                    font-size: 50px;
                    color: #f03c6f;
                    margin-bottom: 26px;
                }
                
                #content-desktop {display: block;}
                #content-mobile {display: none;}
                
                @media screen and (max-width: 768px) {
                    
                    #content-desktop {display: none;}
                    #content-mobile {display: block;width:100%;}
                    #content-mobile img{
                        width: 100%;
                    }
                }
            </style>
            
            <link rel="stylesheet" type="text/css" href="{{asset('frontend/timeline.css')}}">
        </head>
        <body class="home header_sticky onepage">
            <div class="boxed">
                
                <div id="preloader">
                    <div id="preloader-status"></div>
                </div>
                
                <div class="header-wrap">
                    <header id="header" class="header clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="logo logo-top" >
                                        <a href="{{url('/')}}" title="Rano Landing Page">
                                            <img class="site-logo"  src="{{asset('images/logo.svg')}}" alt="INVFEST logo"  data-retina="{{asset('images/logo.svg')}}" />
                                        </a>
                                    </div><!-- /.logo -->
                                    
                                    <a href="{{url('/login')}}" class="button-menu center" target="_blank">Sign In</a>
                                    
                                    <div class="nav-wrap">
                                        <nav id="mainnav" class="mainnav">
                                            <ul class="menu main-menu"> 
                                                <li class="menu-item">
                                                    <a href="#tentang">Tentang</a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="#kompetisi">Kompetisi</a>
                                                </li>
                                                <li class="menu-item">
                                                    <a href="#timeline">Timeline</a>
                                                </li>
                                                <li  class="menu-item">
                                                    <a href="#">Mata Lomba</a>
                                                    <ul class="sub-menu">
                                                        <li class="menu-item"><a href="{{url('/app_innovation')}}">APP Innovation</a></li>
                                                        <li class="menu-item"><a href="{{url('/ui_ux_exploration')}}">UI/UX Exploration</a></li>
                                                    </ul>
                                                </li>
                                                <!-- <li class="menu-item">
                                                    <a href="#">Pengumuman</a>
                                                </li> -->
                                                <li class="menu-item">
                                                    <a href="#daftar" class="menu-item">Sign Up</a>
                                                </li>
                                            </ul>
                                        </nav><!-- #site-navigation -->
                                        <div class="btn-menu">
                                            <span></span>
                                        </div><!-- //mobile menu button -->
                                    </div><!-- /.nav-wrap -->
                                    
                                </div><!-- /.col-sm-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </header><!-- /.header -->
                </div> <!-- /.header-wrap -->
                
                <div class="page-wrap home-1" id="content-desktop">
                    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery">
                        <!-- START REVOLUTION SLIDER 5.4.7.3 fullwidth mode -->
                        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner font-eina" data-version="5.4.7.3">
                            <ul>    <!-- SLIDE  -->
                                <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide">
                                    <!-- MAIN IMAGE -->
                                    <img src="{{asset('frontend/images/slide.jpg')}}"  alt="image" title="slide"  width="1920" height="1000" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->
                                    
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-resizeme title-revslider"
                                    id="slide-1-layer-1"
                                    data-x="center" data-hoffset="1"
                                    data-y="center" data-voffset="[-202,-202,-202,-100]" data-width="['auto']"
                                    data-height="['auto']" data-type="text" data-responsive_offset="on"
                                    data-frames='[{"delay":10,"speed":1120,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                    data-textAlign="['center','center','center','center']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]">
                                    " Innovation Key to Growth<br/> Economy with Financial <br/>Technology "
                                </div>
                                
                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption tp-resizeme subtitle-revslider"
                                id="slide-1-layer-2"
                                data-x="center" data-hoffset=""
                                data-y="center" data-voffset="[-106,-106,-106,-5]" data-width="['auto']"
                                data-height="['auto']" data-type="text" data-responsive_offset="on"
                                data-frames='[{"delay":540,"speed":1150,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]">
                            </div>
                            
                            <!-- LAYER NR. 3 -->
                            
                            <a href="{{ url('/register') }}" target="_blank">
                                <div class="tp-caption rev-btn rev-withicon  tp-resizeme button btn-slider"
                                id="apple-btn"
                                data-x="center" data-hoffset="[-108,-108,-108,-108]"
                                data-y="center" data-voffset="[-13,-13,-13,70]"
                                data-width="['160']"
                                data-height="['auto']"
                                
                                data-type="button"
                                data-responsive_offset="on"
                                
                                data-frames='[{"delay":1540,"speed":1500,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},
                                
                                {"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"},
                                
                                {"frame":"hover","speed":"300","ease":"Power3.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
                                data-textAlign="['center','center','center','center']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[30,30,30,30]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[20,20,10,0]">
                                <span class="font-maison"><i class="fa fa-wpforms"></i> Daftar</span> 
                            </div>
                        </a>
                        
                        <!-- LAYER NR. 4 -->
                        <a href="#tentang">
                            <div class="tp-caption rev-btn rev-withicon  tp-resizeme button btn-slider"
                            id="android-btn"
                            data-x="center" data-hoffset="[104,104,104,104]"
                            data-y="center" data-voffset="[-13,-13,-13,70]"
                            data-width="['auto']"
                            data-height="['auto']"
                            
                            data-type="button"
                            data-responsive_offset="on"
                            
                            data-frames='[{"delay":1540,"speed":1500,"frame":"0","from":"x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},
                            
                            {"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"},
                            
                            {"frame":"hover","speed":"300","ease":"Power3.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;"}]'
                            data-textAlign="['center','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[30,30,30,30]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[20,20,10,0]">
                            <span class="font-maison"><i class="fa fa-question-circle"></i> Tentang INVFEST</span> 
                        </div>
                    </a>
                    
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption   tp-resizeme"
                    id="slide-1-layer-3"
                    data-x="center" data-hoffset="9"
                    data-y="580"
                    data-width="['none','none','none','none']"
                    data-height="['none','none','none','none']"
                    
                    data-type="image"
                    data-responsive_offset="on"
                    
                    data-frames='[{"delay":2290,"speed":1650,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textAlign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]">
                    <img src="{{asset('frontend/images/piala.svg')}}" alt="mobile image" data-ww="450px"  width="300"  data-no-retina> 
                </div>
            </li>
        </ul>
    </div>
</div>
</div><!-- END REVOLUTION SLIDER -->

<div id="content-mobile">
    <img src="{{asset('frontend/images/mobile.jpg')}}" alt="" srcset="">
</div>

<section class="mobile-feature mf1" id="tentang">
    <div class="container">
        <!-- <div class="row flat-row">
            <div class="col-lg-6 col-sm-12 section-left">
                <div class="video-wrap" style="height: auto;
                margin-top: 180px;">
                <a  class="fancybox" data-type="iframe" href="#">
                    <img  src="{{asset('frontend/images/video.jpg')}}" width="1170" height="650" alt="image">
                    <div class="icon-play">
                        <span class="icon-noun_498864_cc"></span>
                    </div>
                </a>
            </div>
        </div> -->
        
        <div class="col-lg-12 col-sm-12 col-lg-offset-2 section-right center">
            <div class="title-box ">
                <div class="icon-title">
                    <span class="fa fa-question-circle"></span>
                </div>
                <div class="title-section style1">
                    <h2 class="title">
                        Apa itu INVFEST 4.0 ?
                    </h2>
                </div>
                <div class="title-content" style="text-align:justify">
                    <p>INVFEST ( Informatics Innovation Festival ) yaitu acara kompetisi nasional tahunan yang ke empat, pada tahun ini bertemakan "Innovation Key to Growth Economy with Financial Technology". INVFEST merupakan salah satu program kerja dari HMIF ( Himpunan Mahasiswa Informatika ) divisi Minat dan Bakat.</p>
                    <p>Acara ini memiliki tujuan untuk mengembangkan ekonomi Indonesia yang lebih maju dengan adanya teknologi finansial yang nantinya akan berguna bagi masyarakat sekitar. Selain itu, juga untuk mengembangkan kemampuan pemuda pemudi untuk berfikir lebih inovatif dan siap menghadapi persaingan ekonomi mendatang di bidang teknologi.</p>
                    
                </div>
            </div><!-- /.title-box -->
        </div><!-- /.col-sm-5 -->
        <div class="col-sm-12 col-lg-1"></div>
    </div><!-- /.row -->
</div><!-- /.container -->
</section><!-- /.mobile-feature -->

<div class="container">
    <hr>
</div>

<section id="kompetisi" class="mobile-feature mf2">
    <div class="container">
        <div class="row flat-row">
            <div class="col-lg-12 col-sm-12 center">
                <div class="title-box">
                    <div class="icon-title">
                        <span class="fa fa-trophy"></span>
                    </div>
                    <div class="title-section style1">
                        <h2 class="title">
                            Kompetisi
                        </h2>
                    </div>
                    <div class="title-content">
                        <p>Terdapat dua kompetisi yang akan dilombakan yaitu Apps Innovation dan UI/UX Exploration.
                        </p>
                    </div>
                </div><!-- /.title-box -->
                
                <div class="row justify-content-md-center">
                    <div class="col-lg-8 col-sm-12">
                        <div class="card-deck">
                            <div class="card">
                                <img class="card-img-top" src="{{asset('frontend/images/app-dev.png')}}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Apps Innovation</h5>
                                    <p class="card-text">Buatlah Aplikasi Mobile atau Web Mobile yang telah siap untuk dipakai.</p>
                                    <br> <br>
                                    <p>Total Hadiah :</p>
                                    <h4>Rp.7.500.000</h4>
                                    <a href="{{url('/app_innovation')}}" class="btn-rulebook" target="_blank">Selengkapnya</a>
                                    <a href="{{url('/rulebook/rulebook_app_innovation_mahasiswa.pdf')}}" class="btn-rulebook" target="_blank">Download Rulebook</a>
                                </div>
                            </div>
                            <div class="card">
                                <img class="card-img-top" src="{{asset('frontend/images/uiux.png')}}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">UI / UX Exploration</h5>
                                    <p class="card-text">Buatlah rancangan design atau prototype untuk aplikasi atau website semenarik mungkin dan kreatif.</p>
                                    <br>
                                    <p>Total Hadiah :</p>
                                    <h4>Rp.5.500.000</h4>
                                    <a href="{{url('/ui_ux_exploration')}}" class="btn-rulebook" target="_blank">Selengkapnya</a>
                                    <a href="{{asset('/rulebook/rulebook_ui_ux_exploration_pelajar.pdf')}}" class="btn-rulebook" target="_blank">Download Rulebook</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div><!-- /.col-sm-5-->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.mobile-feature -->

<div class="container">
    <hr>
</div>

<section id="timeline" style="padding:50px;">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-12 col-sm-12 center">
                <div class="row justify-content-md-center">
                    <div class="col-lg-12 col-sm-12">
                        <div class="title-box">
                            <div class="icon-title">
                                <span class="fa fa-calendar"></span>
                            </div>
                            <div class="title-section style1">
                                <h2 class="title">
                                    Timeline
                                </h2>
                            </div>
                        </div>
                        
                        <div class="page">
                            <div class="page__demo">
                                <div class="main-container page__container">
                                    <div class="timeline">
                                        <div class="timeline__group">
                                            <span class="timeline__year">2019</span>
                                            
                                            <div class="timeline__box">
                                                <div class="timeline__date">
                                                    <span class="timeline__day">1</span>
                                                    <span class="timeline__month">AGU</span>
                                                </div>
                                                <div class="timeline__post">
                                                    <div class="timeline__content">
                                                        <p>Pendaftaran Gelombang 1</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="timeline__box">
                                                <div class="timeline__date">
                                                    <span class="timeline__day">1</span>
                                                    <span class="timeline__month">SEP</span>
                                                </div>
                                                <div class="timeline__post">
                                                    <div class="timeline__content">
                                                        <p>Pendaftaran Gelombang 2</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="timeline__box">
                                                <div class="timeline__date">
                                                    <span class="timeline__day">1</span>
                                                    <span class="timeline__month">OKT</span>
                                                </div>
                                                <div class="timeline__post">
                                                    <div class="timeline__content">
                                                        <p>Maksimal Pengumpulan Proposal & Aplikasi</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="timeline__box">
                                                <div class="timeline__date">
                                                    <span class="timeline__day">2</span>
                                                    <span class="timeline__month">OKT</span>
                                                </div>
                                                <div class="timeline__post">
                                                    <div class="timeline__content">
                                                        <p>Penilaian dan Penyisihan</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="timeline__box">
                                                <div class="timeline__date">
                                                    <span class="timeline__day">8</span>
                                                    <span class="timeline__month">NOV</span>
                                                </div>
                                                <div class="timeline__post">
                                                    <div class="timeline__content">
                                                        <p>Pengumuman Finalis</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="timeline__box">
                                                <div class="timeline__date">
                                                    <span class="timeline__day">23</span>
                                                    <span class="timeline__month">NOV</span>
                                                </div>
                                                <div class="timeline__post">
                                                    <div class="timeline__content">
                                                        <p>Finalis & Awarding</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div><!-- /.page-wrap -->

<div class="container">
    <div class="col-lg-12">
        <hr/>
    </div>
</div>
<section id="Sponsor" class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 center">
                <div class="title-box">
                    <div class="title-section style1">
                        <h2 class="title">
                            Sponsorship
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center mt-5">
            <div class="col-lg-3">
                <h3 style="margin-top: 20px;">- OPEN SPONSOR -</h3>
            </div>    
        </div>
    </div>
</section>
<div class="container" style="margin-top: 100px;">
    <div class="col-lg-12">
        <hr/>
    </div>
</div>
<section id="media" class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 center">
                <div class="title-box">
                    <div class="title-section style1">
                        <h2 class="title">
                            Media Partner
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center mt-5">
            <div class="col-lg-3">
                <img src="{{asset('frontend/images/media/satelit_tv.jpg')}}" alt="Satelit TV" />
            </div>    
        </div>
    </div>
</section>

<footer class="flat-call-back" id="daftar">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-12">
                <div class="text-call-back">
                    <div class="title">
                        <h1>Ayo <strong>daftar dan buktikan</strong> tim kamu menjadi juara !</h1>
                    </div>
                    <div class="button-call-back center">
                        <a href="{{url('/register')}}" id="sub-button" target="_blank">Daftar</a>
                    </div>
                </div>
            </div><!-- /.col-md-12 -->
            
        </div><!-- /.row -->
    </div><!-- /.container -->
</footer><!-- /.flat-call-back -->

<div class="bottom">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-4 col-sm-12">
                <div id="logo-footer" class="logo-ft" >
                    <img class="site-logo-ft" src="{{asset('frontend/images/logo.svg')}}" alt="image" data-retina="{{asset('frontend/images/logo.svg')}}">
                </div>
            </div><!-- /.col-lg-4 col-sm-12 -->
            
            <div class="col-lg-4 col-sm-12">
                <div class="copyright">
                    <p>© <a href="#">INVFEST</a> 2019. All rights reserved.</p>
                </div>
            </div><!-- /.col-lg-4 col-sm-12 -->
            
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.bottom -->

<div class="go-top" title="Go top">
    <i class="fa fa-chevron-up" aria-hidden="true"></i>
</div><!-- /.go top -->

</div><!-- /.boxed -->


<!-- Fancybox -->
<script  src="{{asset('frontend/fancybox/jquery.fancybox.js')}}"></script>
<script  src="{{asset('frontend/fancybox/jquery.fancybox.pack.js')}}"></script>

<script  src="{{asset('frontend/js/jquery.easing.js')}}"></script>
<script  src="{{asset('frontend/js/jquery.cookie.js')}}"></script>
<script  src="{{asset('frontend/js/main.js')}}"></script>

<!-- Counter Number -->
<script src="{{asset('frontend/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.counterup.js')}}"></script>

<!-- flipster -->
<!-- <script src="{{asset('frontend/js/jquery.flipster.min.js')}}"></script> -->

<!-- Revolution Slider -->
<script  src="{{asset('frontend/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script  src="{{asset('frontend/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script> -->
<script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script> -->
<script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.video.min.js')}}"></script> -->
<script src="{{asset('frontend/js/rev-slider.js')}}"></script>

<!-- Carousel -->
<!-- <script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('frontend/js/owl-carousel.js')}}"></script> -->
</body>
</html>
