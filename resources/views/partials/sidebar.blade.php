<aside class="main-sidebar sidebar-dark-primary elevation-4">
    
    <a href="{{ url('/') }}" class="brand-link">
        <img src="{{ asset('images/brand_logo.png') }}"
            alt="INVFEST 4.0 Logo"
            class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">INVFEST 4.0</span>
    </a>

    <div class="sidebar">
        
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="{{ url('participants/profile') }}" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{ url('participants/dashboard') }}" class="nav-link active">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('participants/payment') }}" class="nav-link active">
                        <i class="nav-icon fa fa-dollar"></i>
                        <p>Payment</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('participants/event') }}" class="nav-link active">
                        <i class="nav-icon fa fa-calendar"></i>
                        <p>Event</p>
                    </a>
                </li>
                
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link active">
                        <i class="nav-icon fa fa-gear"></i>
                        <p>
                            Setting
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('participants/profile') }}" class="nav-link">
                                <i class="fa fa-user nav-icon"></i>
                                <p>Profile</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('participants/team') }}" class="nav-link">
                                <i class="fa fa-group nav-icon"></i>
                                <p>Team</p>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
        
    </div>
    
</aside>