@extends('layouts.administrator')

@section('title')
    Dashboard
@endsection

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-header">
                <h3 class="card-title">Data Tim</h3>

                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="card-body table-responsive p-0">

                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>Nama Tim</th>
                            <th class="text-center">Bukti Pembayaran</th>
                            <th class="text-center">Legalitas</th>
                            <th class="text-center">Proposal</th>
                            <th class="text-center">Aplikasi</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        <tr>
                            <td>Old Reliable</td>
                            <td class="text-center">
                                <button class="btn btn-flat btn-primary btn-sm" title="view">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                            <td class="text-center">
                                <button class="btn btn-flat btn-success btn-sm" title="view">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                            <td class="text-center">
                                <button class="btn btn-flat btn-info btn-sm" title="view">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                            <td class="text-center">
                                <button class="btn btn-flat btn-warning btn-sm" title="download">
                                    <i class="fa fa-download"></i>
                                </button>
                            </td>
                            <td class="text-center">
                                <button class="btn btn-primary btn-flat btn-sm disabled" style="text-transform: uppercase;">
                                    actived
                                </button>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning">Tentang Tim</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Bilscode Team</td>
                            <td class="text-center">
                                <button class="btn btn-flat btn-primary btn-sm" title="view">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                            <td class="text-center">
                                <i class="icon fa fa-exclamation-triangle fa-2x" style="color:red;"></i>
                            </td>
                            <td class="text-center">
                                <i class="icon fa fa-exclamation-triangle fa-2x" style="color:red;"></i>
                            </td>
                            <td class="text-center">
                                <i class="icon fa fa-exclamation-triangle fa-2x" style="color:red;"></i>
                            </td>
                            <td class="text-center">
                                <button class="btn btn-info btn-flat btn-sm disabled" style="text-transform: uppercase;">
                                    process
                                </button>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary">Aktifkan Tim</button>
                                    <button type="button" class="btn btn-warning">Tentang Tim</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>X-men Team</td>
                            <td class="text-center">
                                <i class="icon fa fa-exclamation-triangle fa-2x" style="color:red;"></i>
                            </td>
                            <td class="text-center">
                                <i class="icon fa fa-exclamation-triangle fa-2x" style="color:red;"></i>
                            </td>
                            <td class="text-center">
                                <i class="icon fa fa-exclamation-triangle fa-2x" style="color:red;"></i>
                            </td>
                            <td class="text-center">
                                <i class="icon fa fa-exclamation-triangle fa-2x" style="color:red;"></i>
                            </td>
                            <td class="text-center">
                                <button class="btn btn-danger btn-flat btn-sm disabled" style="text-transform: uppercase;">
                                    unactive
                                </button>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning">Tentang Tim</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            
        </div>

    </div>

</div>

@endsection


