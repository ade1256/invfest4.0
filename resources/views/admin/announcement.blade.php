@extends('layouts.administrator')

@section('title')
    Announcement
@endsection

@section('content')

<div class="row">

    <div class="col-12">

        @include('partials._alerts')

        <div class="card">

            <div class="card-header">
                <h3 class="card-title">Pengumuman</h3>

                <div class="card-tools">
                    <button class="btn btn-primary btn-flat btn-sm" data-toggle="modal" data-target="#modal-lg">
                        <i class="fa fa-plus"></i> Tambah
                    </button>
                    <div class="modal fade" id="modal-lg">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h4 class="modal-title">Tambah Pengumuman</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                    
                                <form action="{{ route('addAnnouncement') }}" method="post" >

                                    <div class="modal-body">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <textarea class="form-control" name="announcement" cols="30" rows="10" placeholder="Tulis Pengumuman"></textarea>
                                            @error('announcement')
                                                <strong>{{ $message }}</strong>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                
                                </form>

                            </div>
                            
                        </div>

                    </div>
                    
                </div>
            </div>
            
            <div class="card-body table-responsive p-0">

                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>No</th>
                            <th class="text-center">Pengumuman</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        @foreach($announcement_view as $announ)
                        @php
                            $no=1;
                        @endphp
                        <tr>
                            <td>{{ $no }}</td>
                            <td class="text-center">
                                {{ $announ->announcement }}
                            </td>
                            <td class="text-center">
                                <button class="btn btn-flat btn-danger btn-sm" title="view">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                        @endforeach
                    </tbody>
                </table>

            </div>
            
        </div>

    </div>

</div>

@endsection


