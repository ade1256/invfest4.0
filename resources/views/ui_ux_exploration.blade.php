<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<!-- Basic Page Needs -->
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title>INVFEST 4.0 - 2019 IT Telkom Purwokerto</title>

	<meta name="author" content="invfest">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/revolution/css/settings.css')}}">

    <!-- Boostrap style -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/bootstrap.css')}}">

    <!-- Icommon icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/icommon.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/style.css')}}">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/responsive.css')}}">

    <!-- Carousel -->
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/owl.theme.default.min.css')}}"> -->

    <!-- Flipster -->
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/flipsternavtabs.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/jquery.flipster.min.css')}}"> -->

    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/stylesheet/animate.css')}}">

    <!-- FancyBox -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/fancybox/jquery.fancybox.css')}}" media="screen">

    <!-- Jquery -->
    <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/js/counter.js')}}"></script>

    <style>
        .header{
            background: #35356b;
        }
        .btn-rulebook {
            height: 40px;
            line-height: 40px;
            width: 160px;
            border: 1px solid #f03c6f;
            border-radius: 20px;
            color: #f03c6f;
            font-size: 15px;
            display: inline-block;
            margin-top: 35px;
        }
        .btn-rulebook:hover {
            background-color: #f03c6f;
            color: #ffffff;
        }
        .timelineDot {
            margin:20px 20px !important; 
        }
        .timelineDate {
            margin-left:50px !important; 
            margin-right:50px !important; 
        }
        .timelineWork {
            margin-left:50px!important; 
        }
        .icon-title {
            font-size: 50px;
            color: #f03c6f;
            margin-bottom: 26px;
        }

        #content-desktop {display: block;}
        #content-mobile {display: none;}

        @media screen and (max-width: 768px) {

            #content-desktop {display: none;}
            #content-mobile {display: block;width:100%;}
            #content-mobile img{
                width: 100%;
            }
        }
    </style>
    
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/timeline.css')}}">
</head>
<body class="home header_sticky onepage">
    <div class="boxed">

        <div id="preloader">
            <div id="preloader-status"></div>
        </div>

        <div class="header-wrap">
            <header id="header" class="header clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="logo logo-top" >
                                <a href="{{url('/')}}" title="Rano Landing Page">
                                    <img class="site-logo"  src="{{asset('images/logo.svg')}}" alt="INVFEST logo"  data-retina="{{asset('images/logo.svg')}}" />
                                </a>
                            </div><!-- /.logo -->

                            <a href="{{url('/login')}}" class="button-menu center" target="_blank">Sign In</a>

                            <div class="nav-wrap">
                                <nav id="mainnav" class="mainnav">
                                    <ul class="menu main-menu"> 
                                        <li  class="menu-item">
                                            <a href="#">Mata Lomba</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item"><a href="{{url('/app_innovation')}}">APP Innovation</a></li>
                                                <li class="menu-item"><a href="{{url('/ui_ux_exploration')}}">UI/UX Exploration</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item">
                                            <a href="#daftar" class="menu-item">Sign Up</a>
                                        </li>
                                    </ul>
                                </nav><!-- #site-navigation -->
                                <div class="btn-menu">
                                    <span></span>
                                </div><!-- //mobile menu button -->
                            </div><!-- /.nav-wrap -->

                        </div><!-- /.col-sm-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </header><!-- /.header -->
        </div> <!-- /.header-wrap -->

        <section class="mobile-feature mf1" id="tentang">
            <div class="container">
                <!-- <div class="row flat-row">
                    <div class="col-lg-6 col-sm-12 section-left">
                        <div class="video-wrap" style="height: auto;
                        margin-top: 180px;">
                        <a  class="fancybox" data-type="iframe" href="#">
                        <img  src="{{asset('frontend/images/video.jpg')}}" width="1170" height="650" alt="image">
                            <div class="icon-play">
                                <span class="icon-noun_498864_cc"></span>
                            </div>
                        </a>
                    </div>
                </div> -->

                <div class="col-lg-12 col-sm-12 col-lg-offset-2 section-right center">
                    <div class="title-box "  style="margin-top:5%;">
                        <div class="icon-title">
                            <span class="fa fa-question-circle"></span>
                        </div>
                        <div class="title-section style1">
                            <h2 class="title">
                                Apa itu UI/UX Exploration ?
                            </h2>
                        </div>
                        <div class="title-content" style="text-align:justify">
                            <p>UI/UX Exploration merupakan salah satu kategori yang dilombakan dalam kompetisi nasional INVFEST 4.0</p>
                            <p>Peserta yang mengikutin App Innovation merupakan siswa/i aktif di seluruh Indonesia.</p>
                            <p>UI/UX Exploration merupakan adalah kompetisi desain antarmuka sistem/produk yang berorientasi kepada kenyamanan dan kemudahan pengguna (user) dalam menggunakan sistem/produk tersebut. Tujuan dari design interface ini adalah untuk membuat interaksi pengguna sesederhana dan se-efisien mungkin. Design Interface juga menentukan bagaimana user berinteraksi dengan komputer menggunakan tampilan antarmuka (interface) yang ada pada layar komputer. Dalam kompetisi ini yang menjadi fokus utama adalah membuat antar muka pengguna menjadi efisien dan memberikan pengalaman yang menyeluruh bagi pengguna ketika sedang menggunakan sistem/produk tersebut. Lomba ini terdiri dari 2 (dua) tahap, yaitu tahap penyisihan dan tahap final. Pada tahap penyisihan, tim peserta wajib mengumpulkan proposal. Proposal tersebut akan digunakan sebagai penilaian untuk tahap final. Pada tahap final, tim peserta wajib menyiapkan laporan lengkap dari desain sistem/produk yang diajukan dan mempresentasikannya di depan juri.</p>
                            <p>Dalam perlombaan ini peserta dapat menggunakan software pendukung seperti Adobe Illustrator, Figma, Adobe Photoshop, Sketch, Adobe XD, dan lain sebagainya.</p>
                            <a href="{{asset('/rulebook/rulebook_ui_ux_exploration_pelajar.pdf')}}" class="btn-rulebook" target="_blank">Download Rulebook</a>
                        </div>
                    </div><!-- /.title-box -->
                </div><!-- /.col-sm-5 -->
                <div class="col-sm-12 col-lg-1"></div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.mobile-feature -->

    </body>
    
<!-- Fancybox -->
<script  src="{{asset('frontend/fancybox/jquery.fancybox.js')}}"></script>
<script  src="{{asset('frontend/fancybox/jquery.fancybox.pack.js')}}"></script>

<script  src="{{asset('frontend/js/jquery.easing.js')}}"></script>
<script  src="{{asset('frontend/js/jquery.cookie.js')}}"></script>
<script  src="{{asset('frontend/js/main.js')}}"></script>

<!-- Counter Number -->
<script src="{{asset('frontend/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.counterup.js')}}"></script>

<!-- flipster -->
<!-- <script src="{{asset('frontend/js/jquery.flipster.min.js')}}"></script> -->

<!-- Revolution Slider -->
<script  src="{{asset('frontend/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script  src="{{asset('frontend/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script> -->
<script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script> -->
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script> -->
<script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<!-- <script  src="{{asset('frontend/revolution/js/extensions/revolution.extension.video.min.js')}}"></script> -->
<script src="{{asset('frontend/js/rev-slider.js')}}"></script>

<!-- Carousel -->
<!-- <script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontend/js/owl-carousel.js')}}"></script> -->
</body>
</html>