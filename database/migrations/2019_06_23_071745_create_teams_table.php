<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->bigIncrements('id_team');
            $table->string('team_name');
            $table->string('token',8)->unique();
            $table->integer('leader_id')->unique();
            $table->enum('category', ['uiux', 'app_inv']);
            $table->enum('status', ['actived', 'process', 'unactive'])->default('unactive');
            $table->string('payment')->nullable();
            $table->string('poes')->nullable();
            $table->string('proposal')->nullable();
            $table->string('application')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
