<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/coomingsoon', 'PartialsController@index');
Route::get('/app_innovation', 'PartialsController@app_inv');
Route::get('/ui_ux_exploration', 'PartialsController@uiux');

Route::group(['middleware' => ['auth' , 'verified'], 'prefix' => '/participants' ], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/dashboard','ParticipantsController@participants');
    Route::get('/event','ParticipantsController@event');
    Route::get('/team','ParticipantsController@team');
    Route::get('/profile','ParticipantsController@profile');
    Route::get('/payment','ParticipantsController@payment');

    //Create and Join Team
    Route::post('/create','TeamController@createTeam')->name('createTeam');
    Route::post('/join','TeamController@joinTeam')->name('joinTeam');
    Route::post('/team','TeamController@updateTeamName')->name('updateTeamName');
    
    //Out Team
    Route::post('/outteam','TeamController@outTeam')->name('outTeam');
    //Delete Team
    Route::post('/deleteteam','TeamController@deleteTeam')->name('deleteTeam');

    //Payment
    Route::post('/payment','TeamController@givePayment')->name('givePayment');
    //Legality
    Route::post('/legality','TeamController@giveLegality')->name('giveLegality');
    //Proposal
    Route::post('/proposal','TeamController@giveProposal')->name('giveProposal');
    //Application
    Route::post('/application','TeamController@giveApplication')->name('giveApplication');

    //Update Profile
    Route::post('/profile','UserDetailController@updateParticipants')->name('updateParticipants');
  
});

Route::group(['middleware' => ['auth' /*, 'verified'*/], 'prefix' => '/administrator' ], function () {

    /*
    *
    Admin Dashbaord
    *
    */

    Route::get('/dashboard','AdministratorController@index');
    Route::get('/announcement','AdministratorController@announcement');
    
    //Pengumuman
    Route::post('/announcement','AdministratorController@addAnnouncement')->name('addAnnouncement');

    
});